# Docker nextcloud

This docker set up the following : Redis, Mariadb, Nextcloud (with Apache) and Collabora

The Nextcloud image has a lot of functionnalities added with the Dockerfile, so you have to build it. 
(https://hub.docker.com/_/nextcloud/)
(https://github.com/nextcloud/docker/tree/master/.examples)
Base for Dockerfile : https://github.com/nextcloud/docker/tree/master/.examples/dockerfiles/full/apache
Base for docker-compose : https://github.com/nextcloud/docker/blob/master/.examples/docker-compose/insecure/mariadb/apache/docker-compose.yml

This HAS to work behind a proxy (here traefik), but you can remove traefik labels if you use another one.
You have to set you passwords and domains in the .env file first


#### Create and run home docker as detached
    docker-compose up -d

#### Create and run home docker
    docker-compose up

#### Stop home docker and related network
    docker-compose down

#### Access to logs and follow
    docker-compose logs -f

#### Build and Pull image
    docker-compose build --pull

#### Open terminal on docker container
    docker containerid exec -it dockname /bin/bash

Things should work fine behind a proxy. If you are still experiencing problemes,
you might consider set following environnement vars : 
OVERWRITEHOST, OVERWRITEPROTOCOL, OVERWRITEWEBROOT, OVERWRITECONDADDR
(More explanations on: https://hub.docker.com/_/nextcloud)

After App `Preview Generator` is installed and enabled, you Might consider running these 
commands once the instance is running to avoid very big previews : 
(https://help.nextcloud.com/t/thumbnails-previews-loading-slow-on-files-and-gallery-app/58749)
```
sudo docker exec --user www-data nextcloud php occ config:app:set previewgenerator squareSizes --value="32 256"
sudo docker exec --user www-data nextcloud php occ config:app:set previewgenerator widthSizes --value="256 384"
sudo docker exec --user www-data nextcloud php occ config:app:set previewgenerator heightSizes --value="256"
sudo docker exec --user www-data nextcloud php occ config:system:set preview_max_x --value 2048
sudo docker exec --user www-data nextcloud php occ config:system:set preview_max_y --value 2048
sudo docker exec --user www-data nextcloud php occ config:system:set jpeg_quality --value 60
sudo docker exec --user www-data nextcloud php occ config:app:set preview jpeg_quality --value="60"
```
